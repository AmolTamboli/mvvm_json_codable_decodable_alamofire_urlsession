//
//  ViewController.swift
//  MVVM_JSON_(CODABLE_DECODABLE)_ALAMOFIRE_URLSESSION
//
//  Created by Amol Tamboli on 21/09/20.
//  Copyright © 2020 Amol Tamboli. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var viewUserModel = UserViewModel()
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.register(UINib(nibName: "UserCell", bundle: nil), forCellReuseIdentifier: "UserCell")
        viewUserModel.vc = self
        viewUserModel.getAllUserDataAF()
    }
}

extension ViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewUserModel.arrUsers.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserCell
        let modelUser = viewUserModel.arrUsers[indexPath.row]
        cell .modelUser = modelUser
        return cell
    }
}
