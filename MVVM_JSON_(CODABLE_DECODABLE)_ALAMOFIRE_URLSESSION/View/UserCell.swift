//
//  UserCell.swift
//  MVVM_JSON_(CODABLE_DECODABLE)_ALAMOFIRE_URLSESSION
//
//  Created by Amol Tamboli on 21/09/20.
//  Copyright © 2020 Amol Tamboli. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {

    @IBOutlet weak var lblId: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblStatus: UILabel!

    var modelUser : UserModel?{
        didSet{
            self.configuration()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configuration(){
        if let id = modelUser?.id{
            lblId.text = "\(modelUser?.id)"
        } else {
            lblId.text = "NO ID"
        }
        
        lblTitle.text = modelUser?.title
        let status = modelUser?.gelColorStatus()
        lblStatus.text = status?.0
        backgroundColor = status?.1
    }
}
